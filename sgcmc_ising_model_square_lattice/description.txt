DL_MONTE test case - Semi-grand MC for the Ising model on a square lattice (adapted into an example)


AUTHOR

Tom L Underwood


GENERAL DESCRIPTION

In the Ising model there are two 'species' of atom, spin up and spin down, and atoms 
interact only with their nearest neighbours as follows: pairs of neighbouring atoms i
and j contribute an energy -JS_iS_j to the total energy of the system, where S_i and
S_j are the spins of the atoms i and j, and S_i = +1 if i is spin up and -1 if i is spin
down. Moreover there is an additional contribution to the total energy of the system
associated with the interactions of the spins with an external magnetic field of 
strength H: each atom contributes an additional quantity of energy HS_i to the total 
energy of the system.

The Ising model on a square lattice has been solved for zero external mangetic field [1],
and hence is an excellent testing ground for simulation codes. Here I calculate the
magnetisation per particle at k_BT/J=1.5 with DL_MONTE using the semi-grand functionality. 
The exact expression for the magnetisation is

M = [1-sinh^{-4}(2\beta J)]^{1/8},

where \beta=1/(k_BT). Thus at k_BT=1.5 we have M=0.9864996026. I would expect DL_MONTE to
yield this value for a sufficiently large system. Here I consider a system of 1600 atoms.
The system is initialised with all spins up: species 'U' corresponds to spin up and species
'D' to spin down. Semi-grand moves change the species of atoms from 'U' to 'D' or vice
versa. After equilibration, the M obtained from the simulation should correspond
to the aforementioned value, where M is calculated using

M = (N_u-N_d)/N,

where N_u is the number of up spins in the system, N_d is the number of down spins, and N
is the total number of atoms. 

A simulation was performed using the following version of DL_MONTE: 

commit d66c6d209a6d041416174b5c24e0a6cae6d8aa75
Author: Tom Underwood <t.l.underwood@bath.ac.uk>
Date:   Wed Nov 7 14:20:27 2018 +0000

This commit passes the test because the simulation yielded a magnetisation per particle, M, 
of 0.98653(11), which is within one standard error of the exact value 0.9864996026. The script 
'analysis.sh', in conjunction with the script 'blockav', both contained in the 'long' directory, 
calculates M from the 'YAMLDATA.000' file output by the simulation ('analysis.sh', 'blockav' 
and 'YAMLDATA.000' should all be in the same directory before 'analysis.sh' is invoked). 
This script uses block averaging applied to data post-equilibration to determine M and an 
associated uncertainty. The output of the script for the long simulation was as follows:

$ sh analysis.sh
Number of data points =  2500
Equilibration period =  200
Post-equilibration data points =  2300
Block size =  200
Number of blocks found =  11
Block-averaging mean = 9.8653011363636456e-01
Block-averaving standard error = 1.1478060288777247e-04

The simulation took 43 minutes to run on one node of the Bath HPC facility Balena. 

Here I have also included a Fortran program 'mk_config.f90', which was used to create the 
initial CONFIG file, and could be used to generate CONFIG files for different system sizes
and shapes. See that file for more details.


REFERENCES

[1] L. Onsager, Phys. Rev., Series II, 65, 117 (1944)

