program mk_config

  implicit none

  real :: nndist
  integer :: nx, ny, N, i, j

  write(*,*) "* This program creates a CONFIG file corresponding to a square lattice *"
  write(*,*) "Input nearest neighbour distance: "
  read(*,*) nndist
  write(*,*) "Input number of atoms in x direction: "
  read(*,*) nx
  write(*,*) "Input number of atoms in y direction: "
  read(*,*) ny

  N = nx*ny

  open(unit=10,file="CONFIG")

  write(10,*) "2D lattice gas; nearest neighbour distance ",nndist
  write(10,*) "0 1"
  write(10,*) nndist*nx, 0.0, 0.0
  write(10,*) 0.0, nndist*ny, 0.0
  write(10,*) 0.0, 0.0, 10.0*nndist
  write(10,*) "NUMMOL 1 1"
  write(10,*) "MOLECULE Sys", N, N

  do i = 1, nx
     do j = 1, ny
        write(10,*) "U core"
        write(10,*) "   ", i*nndist, j*nndist, 0.0
     end do
  end do

  close(10)


end program mk_config
