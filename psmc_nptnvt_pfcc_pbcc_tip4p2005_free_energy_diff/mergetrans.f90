! Author : Tom L. Underwood
!
! This program merges the transition matrices contained in multiple TMATRX-format files output by DL_MONTE 
! and outputs the resulting transition matrix to the TMATRX-format file TMATRX_merged. By 'merge' I mean that
! the transition matrices are added together.
! 
! Useage: The command-line arguments to this program are the TMATRX-format files to merge.
! 
! Limitations: The file names must not exceed 100 characters each.
!
program mergetrans

    implicit none

    ! Kinds (match the default for DL_MONTE)
    integer, parameter :: wp = selected_real_kind(14,300)
    integer, parameter :: ip = selected_int_kind(12)

    ! Number of command-line arguments
    integer :: nargs

    ! Array of TMATRX filenames
    character(len=100), allocatable, dimension(:) :: filename

    ! The transition matrix to output
    real(wp), allocatable, dimension(:,:) :: tout

    ! Workspace matrix
    real(wp), allocatable, dimension(:,:) :: twork

    ! Matrix size
    integer :: size

    ! Unimportant variables
    integer :: i, j, n, unit, err
    character(len=10), dimension(5) :: char  ! For reading the second line in TMATRX files


    ! Get number of command-line arugments
    nargs = iargc()

    if(nargs==0) then

        write(0,*) "mergetrans: Error. No command line argument detected. Arguments should be TMATRX files to combine."
        stop 1

    end if

    allocate(filename(nargs))

    ! Read file names from the arguments
    do n = 1, nargs
    
        call getarg(n, filename(n))

    end do


    ! Read the files
    do n = 1, nargs

        write(*,*) "mergetrans: Reading file '"//trim(filename(n))//"'..."

        open(unit, file=trim(filename(n)), status='old', iostat=err)

        if(err/=0) then

            write(0,*) "mergetrans: Error. Problem openning file "//trim(filename(n))
            stop 1

        end if

        ! Skip the first line which contains the number of iterations
        read(unit,*,iostat=err)

        if(err/=0) then

            write(0,*) "mergetrans: Error. Problem 1st line from "//trim(filename(n))
            stop 1

        end if        

        ! Extract the matrix size from the 6th token on the second line
        if(n==1) then

            ! For the first file assume the read size applies to all subsequent files;
            ! and use the size to set up the matrices for this program
            read(unit,*,iostat=err) char, size

            if(err/=0) then

                write(0,*) "mergetrans: Error. Problem reading 2nd line from "//trim(filename(n))
                stop 1

            end if
    
            write(*,*) "mergetrans: Detected matrix size of ", size

            if(size<1) then 

                write(0,*) "mergetrans: Error. Size of matrix < 1"
                stop 1

            else if(size>100000) then

                write(0,*) "mergetrans: Error. Size of matrix > 100000"
                stop 1

            end if

            ! Set up matrices
            allocate(tout(size,size))
            allocate(twork(size,size))

            tout = 0.0_wp

        else

           ! Extract the matrix size from the sixth token on the second line and check it
           ! matches that read from the first file
           read(unit,*,iostat=err) char, i

           if(err/=0) then

               write(0,*) "mergetrans: Error. Problem reading 2nd line from "//trim(filename(n))
               stop 1

           end if

           if(i/=size) then

               write(0,*) "mergetrans: Error. Matrix size in '"//trim(filename(n))// &
                    "' does not match '"//trim(filename(1))//"'"
               stop 1

           end if

        end if

        ! Read the matrix and amend it to 'tout'
        do i = 1, size
            
            ! Use list-directed formatting
            ! The formatted version which matches that output by 'fed_tm_matrix_output' in DL_MONTE's
            ! 'fed_interface_module.f90' is:
            !  read(unit,'(100000e26.17)',iostat=err) ( twork(i,j), j=1,size )
            read(unit,*,iostat=err) ( twork(i,j), j=1,size )

            if(err/=0) then

                write(0,*) "mergetrans: Error. Problem reading matrix from "//trim(filename(n))
                stop 1

            end if
            
        end do

        tout = tout + twork

        close(unit)

        write(*,*) "mergetrans: Finished reading file '"//trim(filename(1))//"'"

    end do


    ! Output the combined matrix to TMATRX_merged
    open(unit, file='TMATRX_merged')

    write(*,*) "mergetrans: Outputting merged transition matrix to 'TMATRX_merged'..."

    write(unit,'(a,i10,a)') "# FED TM matrix after merging with 'mergetrans' program"
    write(unit,'(a,i8,a)')  "# This matrix is for ",size," FED states"

    do i = 1, size
        
        !TU: The 100000 is the maximum size which can be output using the following line
        write(unit,'(100000e26.17)') ( tout(i,j), j=1,size )

    end do


    close(unit)

    write(*,*) "mergetrans: Done"

end program mergetrans
