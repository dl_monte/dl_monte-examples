
# Analyses data from YAMLDATA.000 files from many NVT PSMC simulations, and calculates the free energy difference
# between the phases, the mean volumes of each phase and the mean energies of each phase - adding tail
# corrections. Assumes the YAMLDATA files are in replica_* directories in the final argument:
#
# Argument 1: Number of particles
# Argument 2: equiltime (MC steps)
# Argument 3: one-phase properties block length (MC steps)
# Argument 4: free energy difference block length (MC steps)
# Argument 5: directory coresponding to production simulation

rm -f analysis_yaml.awk.tmp
rm -f replica_analysis.dat

cat > analysis_yaml.awk.tmp << _EOF_
BEGIN{ 

    # User input...

    # Constants for tail corrections for LJ part
    epsilon=0.7749
    sigma=3.1589
    rc=8.5

    # Ensemble details
    N=$1
    beta=1.0/(0.0083144621*440) # 0.0083144621 is k_B in kJ/(mol K)

    # Analysis details
    equiltime=$2     
    onephaseblock=$3
    twophaseblock=$4

    print "Script arguments: N, equilibration time, one-phase block size, FED block size = ",N,equiltime,onephaseblock,twophaseblock

    # For internal use...

    pi=atan2(0,-1)
    tailconst=8*pi*epsilon*sigma**3*((sigma/rc)**9/3-(sigma/rc)**3)/3

    # Corrected counts for each phase correcting for bias (intrablock - two-phase blocks)
    twophasecounts1=0.0
    twophasecounts2=0.0
    # Counts for each phase (intrablock - two-phase blocks)
    twophasehist1=0
    twophasehist2=0

    # Interblock stuff for free energy difference (FED)
    twophaseblockcount=0
    fedsum=0.0
    fedsumsq=0.0
    
    # Corrected counts for each phase correcting for bias (intrablock - one-phase blocks)
    onephasecounts1=0.0
    onephasecounts2=0.0
    # Counts for each phase (intrablock - one-phase blocks)
    onephasehist1=0
    onephasehist2=0
    # The running total for the energy of phases 1 and 2 (intrablock - one- phase blocks)
    energysum1=0.0
    energysum2=0.0

    # Interblock stuff for energy and volume
    onephaseblockcount1=0
    onephaseblockcount2=0
    Energysum1=0.0
    Energysum2=0.0
    Energysumsq1=0.0
    Energysumsq2=0.0

}

# The part of the script for parsing the file
\$1=="timestamp:" {timestamp=\$2}
\$1=="bias:" {bias=\$2}
\$1=="psmcactive:" {psmcactive=\$2}
\$1=="energy:" {energytot=\$2}

    # 'energy' is the last keyword in a data frame

    if(timestamp > equiltime){

        # Get the tail corrections and weight for the data point
        Etail = tailconst*N*N/volume
        weight = exp(bias-beta*Etail)
 
        # Update counts and sums - adding the tail corrections to the energy
        if(psmcactive==1){
 
            twophasecounts1=twophasecounts1+weight
            twophasehist1=twophasehist1+1

            onephasecounts1=onephasecounts1+weight
            onephasehist1=onephasehist1+1

            energysum1=energysum1+weight*(energytot+Etail)
 
        }
        if(psmcactive==2){

            twophasecounts2=twophasecounts2+weight
            twophasehist2=twophasehist2+1

            onephasecounts2=onephasecounts2+weight
            onephasehist2=onephasehist2+1

            energysum2=energysum2+weight*(energytot+Etail)

        }


        # Are we at the end of a block?

        # Two-phase blocks
        if(twophasehist1+twophasehist2>twophaseblock){

            if(twophasecounts1 > 0.0 && twophasecounts2 > 0.0){

                fed = -log(twophasecounts1/twophasecounts2)/N

                fedsum=fedsum+fed
                fedsumsq=fedsumsq+fed*fed

                twophaseblockcount = twophaseblockcount+1

                print "End of FED block : block no., fed = ",twophaseblockcount,fed

            }
            else{

                print "WARNING: Found FED block with no visits to one phase. Ignoring it..."

            }

            twophasecounts1=0.0
            twophasecounts2=0.0
            twophasehist1=0
            twophasehist2=0

        }

        # One-phase blocks
        if(onephasehist1>onephaseblock){

            Energy = energysum1 /(onephasecounts1*N)

            Energysum1 = Energysum1 + Energy
            Energysumsq1 = Energysumsq1 + Energy*Energy

            onephaseblockcount1 = onephaseblockcount1+1

            print "End of phase 1 block : block no., energy = ",onephaseblockcount1, Energy

            onephasecounts1=0.0
            onephasehist1=0
            energysum1=0.0

        }
        if(onephasehist2>onephaseblock){

            Energy = energysum2 /(onephasecounts2*N)

            Energysum2 = Energysum2 + Energy
            Energysumsq2 = Energysumsq2 + Energy*Energy

            onephaseblockcount2 = onephaseblockcount2+1

            print "End of phase 2 block : block no., energy = ",onephaseblockcount2, Energy

            onephasecounts2=0.0
            onephasehist2=0
            energysum2=0.0

        }

 
     }


}

END{

    # Debugging info
    print "Hanging intra-block visits to phase 1 and 2 = ", onephasehist1, onephasehist2
    print "Hanging intra-block visits to phase 1 and 2 (for free energy difference) = ", twophasehist1, twophasehist2
    print "Number of blocks: FED, phase 1 properties, phase 2 properties = ", twophaseblockcount, onephaseblockcount1, onephaseblockcount2

    # Output the block averaging data: Note that the uncertainty is the standard error in the mean with the Bessel-corrected
    # standard deviation used in evaluating the standard error
    if(twophaseblockcount>1){
        printf "Free energy difference, uncertainty = %.17e %.17e\n", fedsum/twophaseblockcount , sqrt( fedsumsq / twophaseblockcount - ( fedsum / twophaseblockcount )**2 ) / sqrt( twophaseblockcount - 1 )
    }
    else{
        print "Not enough blocks for free energy difference"
    }

    if(onephaseblockcount1>1){ 
        printf "Energy phase 1, uncertainty = %.17e %.17e\n", Energysum1/onephaseblockcount1 , sqrt( Energysumsq1 / onephaseblockcount1 - ( Energysum1 / onephaseblockcount1 )**2 ) / sqrt( onephaseblockcount1 - 1)
    }
    else{
        print "Not enough phase 1 block counts for an energy"
    }    
      
    if(onephaseblockcount2>1){ 
        printf "Energy phase 2, uncertainty = %.17e %.17e\n", Energysum2/onephaseblockcount2 , sqrt( Energysumsq2 / onephaseblockcount2 - ( Energysum2 / onephaseblockcount2 )**2 ) / sqrt( onephaseblockcount2 - 1)
    }
    else{
        print "Not enough phase 2 block counts for an energy"
    }    

}


_EOF_


rm -f YAMLDATA.tmp

for d in $5/replica_*; do
    
    echo "Including YAMLDATA in directory $d in analysis..."
    cat $d/YAMLDATA.000 >> YAMLDATA.tmp

done 

echo "Analysing data..."

awk -f analysis_yaml.awk.tmp YAMLDATA.tmp

echo "Cleaning up..."
#rm -f YAMLDATA.tmp
#rm -f analysis_yaml.awk.tmp

echo "Done"

