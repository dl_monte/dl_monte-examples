
Here is a library of example input files for DL_MONTE. Each directory corresponds to a different
example, and within each directory is a file 'description.txt' which describes the example.

Note that many of the examples included here are taken directly from the DL_MONTE test suite.
The 'description.txt' files for such examples may contain technical details pertaining to testing
which are irrelevant here. Moreover some of the input files may be a bit messy. In time we intend
to tidy them up to make them more clear, and hence better examples.

The format of the directory names loosely adheres to the following convention, to make it easier
to find examples relevant to the user. The directory name begins with text describing the 
ensemble, e.g. 'npt', 'gibbs'. This should be followed by text describing the system or model,
e.g. 'water_spc'. Finally there should be text providing any additional information.  


