#!/bin/bash

# Extracts the pressure and an uncertainty once the simulations are completed:
# the first quantity is the pressure in reduced units, and the second is its 
# standard error.

for d in sim_*; do cd $d || exit 1;  python ../python_analysis.py || exit 1 ; cd ..; done | grep pressure | awk '{sum=sum+$4; sum2=sum2+$4*$4; count++} END{mean=sum/count; var=sum2/count-mean**2; printf "%10.9f %10.9f\n", mean,sqrt(var)/sqrt(count)}'
