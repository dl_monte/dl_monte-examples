
# This script takes a FEDDAT file from a completed DL_MONTE simulation to
# examine the liquid-gas transition for the Lennard-Jones fluid, uses
# histogram reweighting to locate coexistence, and outputs various
# coexistence properties: the chemical potential, densities and pressure
# are output to stdout, and the free energy profile is output to
# 'fep_opt.dat'.


import numpy as np

import dlmontepython.fep.fep as fep


# Free energy profile from simulation
filename="FEDDAT.000_120"

# Activity used in simulation
a=0.033911537
# kT used in simulation
kt=1.1
# Volume used in simulation
volume=8**3

# Number of particles in system delineating the gas and liquid phases
nthresh=180



# The script in earnest...


mubeta = np.log(a)
mubetalbound = mubeta - abs(mubeta)*0.01
mubetaubound = mubeta + abs(mubeta)*0.01


op, fe = fep.from_file(filename)

# Reweight to coexistence automatically - no tail corrections
mubeta_opt, p_opt, fe_opt = fep.reweight_to_coexistence(op, fe, mubeta, mubetalbound, mubetaubound/kt, op_thresh=nthresh)
print "mubeta_opt, mu_opt, p_opt = ",mubeta_opt,mubeta_opt*kt, p_opt
fep.to_file(op,fe_opt,"fep_opt.dat")

# Output the gas and liquid densities
op1, op2 = fep.expected_op(op, fe_opt, op_thresh=nthresh)
print "opt phase 1 & 2 rho = ", op1/volume, op2/volume

# Output the pressure 
gsum = np.sum( np.exp(-fe_opt[:nthresh]) )
igsum = np.sum( np.exp(-fe_opt[0]) )
pressure = (kt/volume) * np.log( gsum / igsum )
print "opt pressure = ", pressure
