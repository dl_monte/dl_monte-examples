#!/bin/bash

# Extracts the liquid density and an uncertainty once the simulations are completed:
# the first quantity is the liquid density in reduced units, and the second is its 
# standard error.

for d in sim_*; do cd $d || exit 1;  python ../python_analysis.py || exit 1 ; cd ..; done | grep rho | awk '{sum=sum+$9; sum2=sum2+$9*$9; count++} END{mean=sum/count; var=sum2/count-mean**2; printf "%10.9f %10.9f\n", mean,sqrt(var)/sqrt(count)}'

