#!/bin/sh
#
# Author: Tom L. Underwood
#
# See 'display_help' below for information about this script. 
#




# Function to print information about the script

display_help() {
    echo
    echo "This script performs block averaging on a specified data file, outputting the block-averaging mean and"
    echo "standard error to stdout (along with some other information pertaining to the analysis)."
    echo
    echo "Usage: blockan [-e|--equil <equil>] (-s|--size) <size> <file>"
    echo "       blockan (-s|--size) <size> [-e|--equil <equil>] <file>"
    echo "       blockan (-h|--help)"
    echo
    echo "Here <equil> is the equilibration period and <size> is the block size (which must be provided)."
    echo "For the input file <file> the first column of data is used in the analysis, and it is assumed that there"
    echo "are no empty/comment lines. If not specified, <equil> is set to 0."
    echo
}




# Initilise default variables

EQUIL=0




# Read command-line arguments

if [ $# = 0 ]
then

    display_help
    exit 1

elif [ "$1" = "-h" ] || [ "$1" = "--help" ]
then

    display_help
    exit 0

fi


while [ "$1" != "" ]
do

    case $1 in

        # Equilibration period
        -e|--equil)
            if [ "$2" != "" ]
            then
                EQUIL=$2
                if ! `echo $EQUIL | grep '^[0-9]\+$' > /dev/null`
                then
		    echo "Error: Specified equilibration period '$EQUIL' is not a non-negative integer"
                    exit 1
		fi
            else
                echo "Error: No argument after '-e' or '--equil'"
                exit 1
	    fi
            shift
            shift
            ;;

        # Block size
        -s|--size)
	    if [ "$2" != "" ]
            then
                SIZE=$2
		if ! `echo $SIZE | grep '^[0-9]\+$' > /dev/null`
                then
		    echo "Error: Specified block size '$SIZE' is not a non-negative integer"
                    exit 1
		fi
            else
                echo "Error: No argument after '-s' or '--size'"
                exit 1
	    fi
            shift
            shift
            ;;
        
        # Otherwise we have the file name
        *)
            # First check that the block size has been specified
	    if [ "$SIZE" = "" ]
	    then
	        echo "Error: Block size not specified"
                exit 1
	    fi

            # Check that FILE has not already been set before setting FILE
            if [ "$FILE" = "" ]
            then
                FILE=$1
		if ! [ -f $FILE ]
		then
                    echo "Error: File '$FILE' not found"
                    exit 1		    
		fi
                echo "File = '$FILE'"
                shift
            else
                echo "Error: Unnecessary argument '$1'"
                exit 1
	    fi

    esac

done




# Perform the analysis using awk and output the results

awk -v equil=$EQUIL -v size=$SIZE '

    {
        if(NR > equil){ 
            sum = sum + $1; 
            counts = counts + 1;
            gcounts = gcounts + 1;
            if(counts % size == 0){
                mean = sum / counts; 
                sum = 0; 
                counts = 0; 
                Sum = Sum + mean; 
                Sum2 = Sum2 + mean * mean; 
                Counts = Counts + 1;
            }
        }
    } 

    END{

        print "Number of data points = ",NR
        print "Equilibration period = ",equil
        print "Post-equilibration data points = ",gcounts
        print "Block size = ",size

        if(Counts == 0){
            print "Error: Did not reach end of one block."
            exit 1
        }

        print "Number of blocks found = ",Counts        

        Mean = Sum / Counts;
        printf "Block-averaging mean = %.16e\n",Mean
        
        if(Counts == 1){
            print "Error: Did not reach end of 2 blocks. Cannot calculate standard error."
            exit 1
        }
        else{
            Var = Sum2 / Counts - Mean * Mean;
            Stdev = sqrt(Var*Counts/(Counts-1))
            Stderr =  Stdev / sqrt(Counts);   
            printf "Block-averaving standard error = %.16e\n",Stderr
        }

    }

' $FILE
