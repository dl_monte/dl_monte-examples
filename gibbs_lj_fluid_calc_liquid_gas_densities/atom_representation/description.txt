DL_MONTE test case - Gibbs ensemble simulation of Lennard-Jones fluid - atoms
(adapted to an example)


AUTHOR

Tom L Underwood


GENERAL DESCRIPTION

This directory contains input for DL_MONTE in which the Lennard-Jones fluid is simulated at
T*=1.0 (where T* is the reduced temperature) using Gibbs ensemble Monte Carlo. The Lennard-Jones
potential is truncated at 5*sigma (where 'sigma' is the Lennard-Jones range parameter), and no
tail corrections are applied. Grand-canonical transition-matrix Monte Carlo simulations performed
by NIST [1] give the following values for the vapour and liquid at coexistence at this temperature:
rho*_V=0.03145 and rho*_L=0.6958, where rho* is the reduced density and the uncertainties are
smaller than the quoted number of significant figures. While not strictly a like-for-like comparison,
a Gibbs ensemble simulation using DL_MONTE should yield comparable values.

Note that this test represents the Lennard-Jones particles as 'atoms' in DL_MONTE.

A simulation was performed using a version of DL_MONTE from May 2018, commit
8e131f54f71c829aa8c2b1cc05d01fe0cf649911. The simulation took 2 hours on the Bath HPC facility 
Balena. This simulation passes the test because it yielded rho*_V=0.0312(6) and rho*_L=0.696(2), which
are in excellent agreement with the results in [1]. These quantities were obtained as follows. First
the script 'yamldata_to_rho_atom.sh' was used to generate files 'rho1.dat' and 'rho2.dat', which
contain plots of the density vs. time for the two simulation boxes obtained from the simulation:

$ sh yamldata_to_rho_atom.sh YAMLDATA-1.000 > rho_1.dat
$ sh yamldata_to_rho_atom.sh YAMLDATA-2.000 > rho_2.dat

(Note that the 'rho1.dat' and 'rho2.dat' files output by the simulation are included here).
The script 'blockan.sh' is then used to evaluate the mean and uncertainty in the densities of both
boxes from the files 'rho1.dat' and 'rho2.dat'. I used an equilibration time and block size of 2000
(data points within 'rho1.dat' and 'rho2.dat'); the relevant commands, which output the means and
uncertainties to stdout, are:

$ sh blockan.sh -s 2000 -e 2000 rho1.dat 
$ sh blockan.sh -s 2000 -e 2000 rho2.dat 


REFERENCES

[1] https://www.nist.gov/mml/csd/chemical-informatics-research-group/sat-tmmc-liquid-vapor-coexistence-properties-cut
