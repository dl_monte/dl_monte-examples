This directory contains two examples for a Gibbs ensemble simulation of the Lennard-Jones fluid.
In DL_MONTE a system of 'atom-like' particles can be represented as a collection of atoms 
existing within a large molecule (i.e. the large molecule is the system), or as individual one-atom
molecules within the system. The 'atom_representation' directory corresponds to the former while
the 'molecule_representation' directory corresponds to the latter.

