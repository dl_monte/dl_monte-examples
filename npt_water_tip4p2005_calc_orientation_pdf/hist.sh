#!/bin/sh
#
# Author: Tom L. Underwood
#
# See 'display_help' below for information about this script. 
#




# Function to print information about the script

display_help() {
    echo
    echo "This script takes data in the first column in a specified data file, and outputs a histogram of that data"
    echo "to stdout."
    echo
    echo "Usage: histogram.sh [-h|--help] [-n|--normalise] <min> <max> <bins> <file>"
    echo
    echo "<file> is the file to process. <min> and <max> are the lower and upper limits of the histogram; data"
    echo "outwith these limits are ignored. <bins> is the number of bins the histogram will use. '-n|--normalise'"
    echo "normalises the histogram such that the integral under it is 1."
    echo
}




# Initialise default variables

NORM=0




# Read command-line arguments

if [ $# = 0 ]
then

    display_help
    exit 1

fi


while :
do

    case $1 in

        # Help
        -h|--help)
            display_help
            exit 0
            ;;

        # Specify box type
        -n|--normalise)
            shift         
            NORM=1
            ;;

        # Otherwise we have the type and file name
        *)
            if [ "$1" != "" ]
            then
                MIN=$1
            else
                echo "Error: <min> not specified as argument"
                exit 1
            fi
            if ! `echo $MIN | grep -E '^-?[0-9]+[.]?[0-9]*([eE]-?[0-9]+)?$' > /dev/null`
            then
                echo "Error: <min> is not a number. <min> = '$MIN'"
                exit 1
            fi

            if [ "$2" != "" ]
            then
                MAX=$2
            else
                echo "Error: <max> not specified as argument"
                exit 1
            fi
            if ! `echo $MAX | grep -E '^-?[0-9]+[.]?[0-9]*([eE]-?[0-9]+)?$' > /dev/null`
            then
                echo "Error: <max> is not a number. <max> = '$MAX'"
                exit 1
            fi

            if [ "$3" != "" ]
            then
                BINS=$3
            else
                echo "Error: <bins> not specified as argument"
                exit 1
            fi
            if ! `echo $BINS | grep -E '^[1-9][0-9]*$' > /dev/null`
            then
                echo "Error: <bin> is not a positive integer. <bins> = '$BINS'"
                exit 1
            fi

            if [ "$4" != "" ]
            then
                FILE=$4
            else
                echo "Error: <file> not specified as argument"
                exit 1
            fi
            if ! [ -f $FILE ]
	    then
                echo "Error: File '$FILE' not found"
                exit 1		    
	    fi
            
            break

    esac

done




# Create the histogram

awk -v norm=$NORM -v min=$MIN -v max=$MAX -v bins=$BINS '
    function floor(x){
        ix=int(x)
        # floor(x)=ix if x>=0; floor(x)=ix-1 if x<0 and x!=ix; floor(x)=ix if x<0 and x=ix
        # <=> floor(x)=ix if x=ix || x>=0; floor(x)=ix-1 otherwise
        if( x==ix || x>=0 ){
            return ix
        }
        else{
            return ix-1  
        }
    }

    BEGIN{
        width=(max-min)/bins;
    }

    # NF==0 for blank lines - we skip them
    NF>0 {
        bin=floor(($1-min)/width);
        if(bin>=0 && bin<bins){
            hist[bin]=hist[bin]+1; 
            counts=counts+1;
        }
    }

    END{
        for(i=0;i<bins;i++){
            if(norm==1){                
                print min+(i+0.5)*width, hist[i]/(counts*width);
            }
            else{
                print min+(i+0.5)*width, hist[i]*1.0;
            }
        }
    }

' $FILE

