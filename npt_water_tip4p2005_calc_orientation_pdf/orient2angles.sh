#!/bin/sh
#
# Author: Tom L. Underwood
#
# See 'display_help' below for information about this script. 
#




# Function to print information about the script

display_help() {
    echo
    echo "This script post-processes an ORIENT-format file output by DL_MONTE; it calculates the spherical"
    echo "polar angles phi and theta corresponding to a specified local axis of all molecules in the ORIENT file"
    echo "corresponding to a specified type, and outputs the (phi,theta) pairs for all such molecules to"
    echo "stdout."
    echo
    echo "Usage: orient2angles.sh [-h|--help] [(-b|--box) <box>] [(-e|--equil) <equil>] [(-a|--axis) x|y|z|(lc <a> <b> <c>)] <type> [<file>]"
    echo
    echo "<file> is the ORIENT-format file to post-process. If <file> is not specified then it is assumed"
    echo "to be 'ORIENT.000'. <type> is the label for the molecule in ORIENT to consider. <box> is the"
    echo "box number to consider: only angles for molecules of type <type> in box <box> are output. If"
    echo "<box> is not explicitly specified then it is assumed to be 1. <equil> is the equilibration period:"
    echo "orientations before the equilibration period are ignored. '-a|--axis' specifies the local axis of the"
    echo "molecule to convert to phi and theta. The options are either 'x', 'y' or 'z' (strictly lowercase); or"
    echo "'lc'. 'lc' specifies that phi and theta will be obtained from a linear combination of the x, y and z basis"
    echo "vectors (unit vectors), where <a>, <b> and <c> are the coefficients in the linear combination for the x, y"
    echo "and z vectors respectively. If the '-a|--axis' argument is not invoked then the z axis is used to obtain"
    echo "phi and theta. Note that for '-a|--axis' the options 'x', 'y' and 'lc' are applicable only to molecule"
    echo "types in the ORIENT file which are of the orientation class 'general' (as opposed to 'linear' or"
    echo "'no orientation'. Thus an error is returned if these options are applied to a molecule type not belonging"
    echo "to this class. In a similar vein an error is returned if this script is applied to a molecule in the"
    echo "'no orientation' class."
    echo
}




# Initialise default variables

FILE="ORIENT.000"
BOX=1
AXIS="z"
EQUIL=0




# Read command-line arguments


if [ $# = 0 ]
then

    display_help
    exit 1

fi


while :
do

    case $1 in

        # Help
        -h|--help)
            display_help
            exit 0
            ;;

        # Specify box type
        -b|--box)
            shift
            if [ "$1" != "" ]
            then
                BOX=$1
                shift
                if ! `echo $BOX | grep '^[1-9][0-9]*$' > /dev/null`
                then
	            echo "Error: Specified box '$BOX' is not a positive integer"
                    exit 1
                fi
            else
                echo "Error: No argument after '-b' or '--box'"
                exit 1
            fi
            ;;

        # Specify equilibration period
        -e|--equil)
            shift
            if [ "$1" != "" ]
            then
                EQUIL=$1
                shift
                if ! `echo $EQUIL | grep '^[0-9][0-9]*$' > /dev/null`
                then
	            echo "Error: Specified box '$BOX' is not a non-negatitive integer"
                    exit 1
                fi
            else
                echo "Error: No argument after '-e' or '--equil'"
                exit 1
            fi
            ;;

        # Specify axis to use
        -a|--axis)
            shift
            if [ "$1" = "x" ] || [ "$1" = "y" ] || [ "$1" = "z" ]
            then
                AXIS=$1
                shift
            elif [ "$1" = "lc" ]
            then
                AXIS=$1
                shift
                if [ "$1" != "" ] && [ "$2" != "" ] && [ "$3" != "" ] 
                then
                    LCA=$1
                    LCB=$2
                    LCC=$3
                else
                    echo "Error: <a> <b> and <c> not specified after 'lc'."
                    exit 1
                fi
                if ! `echo $LCA | grep -E '^-?[0-9]+[.]?[0-9]*([eE]-?[0-9]+)?$' > /dev/null`
                then
                    echo "Error: <a> is not a number. <a> = '$LCA'"
                    exit 1
                fi
                if ! `echo $LCB | grep -E '^-?[0-9]+[.]?[0-9]*([eE]-?[0-9]+)?$' > /dev/null`
                then
                    echo "Error: <b> is not a number. <b> = '$LCB'"
                    exit 1
                fi
                if ! `echo $LCC | grep -E '^-?[0-9]+[.]?[0-9]*([eE]-?[0-9]+)?$' > /dev/null`
                then
                    echo "Error: <c> is not a number. <c> = '$LCC'"
                    exit 1
                fi
                shift
                shift
                shift
            else
                echo "Error: Axis argument after '-a' or '--axis' must be 'x', 'y' or 'z'"
                exit 1
            fi
            ;;

        # Otherwise we have the type and file name
        *)
            if [ "$1" != "" ]
            then
                TYPE=$1
            else
                echo "Error: <type> not specified as argument"
                exit 1
            fi

            if [ "$2" != "" ]
            then
                FILE=$2
            fi
            
            break

    esac

done




# Perform the post-processing

awk -v chosentype=$TYPE -v chosenbox=$BOX -v equil=$EQUIL -v chosenaxis=$AXIS -v lca=$LCA -v lcb=$LCB -v lcc=$LCC '

    BEGIN{ 

        if(chosenaxis=="x"){
            axis=1
        }
        else if(chosenaxis=="y"){
            axis=2
        }
        else if(chosenaxis=="z"){
            axis=3
        }
        else if(chosenaxis=="lc"){
            axis=0                    # Note that `axis` is 0 if `lc` is in use
        }
        else{
            print "Error: Axis argument unrecognised in awk."
            exit 1
        }

    }

    # "Iteration" as the first token signifies the start of a data frame
    $1=="Iteration" {

        iteration=$2
        box=$5

        # Skip the next 3 lines which contain the lattice vectors and the number of molecules
        getline
        getline
        getline
        getline

        # We are now on the line containing the number of molecules. Extract this and proceed
        N=$1

        # We are now on to the section of the data frame containing molecular data: N subframes,
        # each containing a molecules COM and, if relevant, orientation. Read all subframes
        for(i=1; i<=N; i++){

            getline

            # Get the type and the orientation class: "no orientation", "linear" or "general"
            # For "no orientation" the subframe corresponding to this molecule contains the COM only, 
            # for "linear" the subframe contains the COM then the local z axis for the molecule;
            # for "general the subframe contains the COM then the local x, y, and z axes for the
            # molecule.
            type=$1
            class=$2
            if(class=="no"){
                # Skip COM
                getline
                getline
                if(type==chosentype && box==chosenbox){
                    print "Error: Found molecule of chosen type with no orientation"
                    exit 1
                }
            }
            else if(class=="linear"){
                # Skip COM
                getline
                getline
                # Calculate phi and theta from the z axis for the chosen type
                if(type==chosentype && box==chosenbox && iteration>=equil){
                    if(axis==3){
                        phi=atan2($2,$1)
                        # zr = z/r
                        zr=$3/sqrt($1*$1+$2*$2+$3*$3)
                        # theta=arccos(zr); arccos(v)=arctan(sqrt(1-v^2)/v) [there is no intrinsic arccos in awk]
                        theta=atan2(sqrt(1-zr*zr),zr)
                        print phi, theta
                    }
                    else{
                        print "Error: Found molecule of chosen type which is linear, and axis is not `z`"
                        exit 1 
                    }
                }
            }
            else if(class=="general"){
                # Skip COM line
                getline
                getline
                # Read x, y and z basis vectors from the subsequent 3 lines
                for(j=1;j<=3;j++){xb[j]=$j}
                getline
                for(j=1;j<=3;j++){yb[j]=$j}
                getline
                for(j=1;j<=3;j++){zb[j]=$j}

                # Perform the relevant calculations if we have a molecule we are interested in...
                if(type==chosentype && box==chosenbox && iteration>=equil){
                    # Set the vector we are interested in
                    if(axis==1){
                        # x axis
                        v[1]=xb[1]
                        v[2]=xb[2]
                        v[3]=xb[3]
                    }
                    else if(axis==2){
                        # y axis
                        v[1]=yb[1]
                        v[2]=yb[2]
                        v[3]=yb[3]
    
                    }
                    else if(axis==3){
                        # z axis
                        v[1]=zb[1]
                        v[2]=zb[2]
                        v[3]=zb[3]
                    }
                    else if(axis==0){
                        # Linear combination
                        v[1]=lca*xb[1]+lcb*yb[1]+lcc*zb[1]
                        v[2]=lca*xb[2]+lcb*yb[2]+lcc*zb[2]
                        v[3]=lca*xb[3]+lcb*yb[3]+lcc*zb[3]
                    }
                    # Calculate phi and theta for the vector
                    phi=atan2(v[2],v[1])
                    # zr = z/r (where z here is v[3] and r is the length ov v[:])
                    zr=v[3]/sqrt(v[1]*v[1]+v[2]*v[2]+v[3]*v[3])
                    # theta=arccos(zr); arccos(p)=arctan(sqrt(1-p^2)/p) [there is no intrinsic arccos in awk]
                    theta=atan2(sqrt(1-zr*zr),zr)
                    print phi, theta
                }
            }
            else{
                print "Error: Molecular class `"class"` unrecognised"
                exit 1
            }
        }
    }

' $FILE

