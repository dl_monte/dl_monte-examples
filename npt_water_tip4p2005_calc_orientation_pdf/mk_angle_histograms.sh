# Author: Tom L. Underwood

# Makes angle histograms for the ORIENT.000 file from the TIP4P/2005 simulation

# Axes corresponding to the 1st and 2nd OH bonds
# Using native axes
a1x=0.0
a1y=0.585882277
a1z=-0.756950327
a2x=0.0
a2y=0.585882277
a2z=0.756950327

# Equilibration time
equil=6000000

# Location of orient2angles.sh and hist.sh scripts
path=`pwd`

echo "Path to support scripts = '$path'"
echo "Equilibration time = $equil"
echo "a1 = $a1x $a1y $a1z"
echo "a2 = $a2x $a2y $a2z"

echo "Examining OH theta angles in ORIENT.000..."
# Get the (phi,theta) for the OH bonds from the O to the 1st H, extract the theta and convert to degrees (via the awk bit), and put in temp.dat
sh $path/orient2angles.sh -e $equil -a lc $a1x $a1y $a1z water ORIENT.000 | awk '{print $2*180/3.1415927}' > temp.dat
# Get the (phi,theta) for the OH bonds from the O to the 2nd H, extract the theta and convert to degrees (via the awk bit), and put in temp.dat
sh $path/orient2angles.sh -e $equil -a lc $a2x $a2y $a2z water ORIENT.000 | awk '{print $2*180/3.1415927}' >> temp.dat

echo "Making 'OH_theta.dat' histogram..."
# Construct a histogram for the OH bonds' theta 
sh $path/hist.sh -n 0 180 100 temp.dat > OH_theta.dat

echo "Making 'OH_theta_corrected.dat'..."
# Correct for the sin(theta) DOS 
awk '{print $1,$2/sin($1*3.14159265359/180)}' OH_theta.dat > OH_theta_corrected.dat.tmp
factor=`awk '{sum=sum+$2;} END{print sum*1.8}' OH_theta_corrected.dat.tmp`
awk -v factor=$factor '{print $1,$2/factor}' OH_theta_corrected.dat.tmp > OH_theta_corrected.dat
rm -f OH_theta_corrected.dat.tmp

echo "Examining OH phi angles in ORIENT.000..."
# Get the (phi,theta) for the OH bonds from the O to the 1st H, extract the phi and convert to degrees (via the awk bit), and put in temp.dat
sh $path/orient2angles.sh -e $equil -a lc $a1x $a1y $a1z water ORIENT.000 | awk '{print $1*180/3.1415927}' > temp.dat
# Get the (phi,theta) for the OH bonds from the O to the 2nd H, extract the phi and convert to degrees (via the awk bit), and put in temp.dat
sh $path/orient2angles.sh -e $equil -a lc $a2x $a2y $a2z water ORIENT.000 | awk '{print $1*180/3.1415927}' >> temp.dat

echo "Making 'OH_phi.dat' histogram..."
# Construct a histogram for the OH bonds' phi
sh $path/hist.sh -n -180 180 100 temp.dat > OH_phi.dat

# Plot the OH bonds' theta and phi in the terminal
gnuplot << _EOF_
set terminal dumb
plot "OH_theta.dat"
plot "OH_theta_corrected.dat"
plot "OH_phi.dat"
_EOF_

#rm -f temp.dat


